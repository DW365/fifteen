﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fifteen
{
    class Game
    {
        int[,] field = new int[4, 4];
        int freePosX = 3, freePosY = 3;
        public Game()
        {
            FieldGen();
            FieldDraw();
            Console.BackgroundColor = Globals.HelpBackgroundColor;
            Console.SetCursorPosition(Globals.ConsoleWidth-4, Globals.ConsoleHeight-3);
        }
        private void FieldGen()
        {
            Random generator = new Random(Convert.ToInt32(DateTime.Now.Ticks%int.MaxValue));
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    field[i, j] = i * 4 + j + 1;
                }
            }
            if (Globals.GenVizualization) FieldDraw();
            for (int i = 0; i < Globals.RandomizeSteps; i++)
            {
                int stepX = 0, stepY = 0;
                int r = generator.Next(4);
                if (freePosX < 3)
                    if (r == 0) stepX++;
                if (freePosX > 0)
                    if (r == 1) stepX--;
                if (freePosY < 3)
                    if (r == 2) stepY++;
                if (freePosY > 0)
                    if (r == 3) stepY--;
                if (Globals.GenVizualization) Drawing.Move(freePosX + stepX, freePosY + stepY, freePosX, freePosY);
                int v = field[freePosY + stepY, freePosX + stepX];
                field[freePosY + stepY, freePosX + stepX] = field[freePosY, freePosX];
                field[freePosY, freePosX] = v;
                freePosY += stepY;
                freePosX += stepX;
            }
            while(freePosX < 3 || freePosY < 3)
            {
                int stepX = 0, stepY = 0;
                if (freePosX < 3) stepX++;
                else stepY++;
                if (Globals.GenVizualization) Drawing.Move(freePosX + stepX, freePosY + stepY, freePosX, freePosY);
                int v = field[freePosY + stepY, freePosX + stepX];
                field[freePosY + stepY, freePosX + stepX] = field[freePosY, freePosX];
                field[freePosY, freePosX] = v;
                freePosY += stepY;
                freePosX += stepX;
            }
        }
        public void Start()
        {
            for(;;)
            {
                var key = Console.ReadKey();
                int stepX = 0;
                int stepY = 0;
                if(key.Key == ConsoleKey.UpArrow)
                    if(freePosY < 3)
                        stepY = 1;
                if (key.Key == ConsoleKey.DownArrow)
                    if (freePosY > 0)
                        stepY = -1;
                if (key.Key == ConsoleKey.LeftArrow)
                    if (freePosX < 3)
                        stepX = 1;
                if (key.Key == ConsoleKey.RightArrow)
                    if (freePosX > 0)
                        stepX = -1;
                if (key.Key == ConsoleKey.Spacebar)
                {
                    Globals.RandomColors();
                    Drawing.Init();
                    FieldDraw();
                }
                Drawing.Move(freePosX + stepX, freePosY + stepY, freePosX, freePosY);
                int v = field[freePosY + stepY, freePosX + stepX];
                        field[freePosY + stepY, freePosX + stepX] = field[freePosY,freePosX];
                        field[freePosY, freePosX] = v;
                freePosY += stepY;
                freePosX += stepX;
                Console.BackgroundColor = Globals.HelpBackgroundColor;
                Console.SetCursorPosition(Globals.ConsoleWidth - 4, Globals.ConsoleHeight - 3);
                if (TestGame()) Drawing.VictoryScreen();
            } 
        }
        private bool TestGame()
        {
            bool r = true;
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    if (i * 4 + j + 1 != field[i, j]) r = false;
                }
            }
            return r;
        }
        private void FieldDraw()
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Drawing.DrawTile(j * (Globals.DefaultTileWidth + 1) + 2, i * (Globals.DefaultTileHeight + 1) + 1, field[i,j]);
                }
            }
        }
    }
}
