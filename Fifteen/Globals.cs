﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fifteen
{
    static class Globals
    {
        public const string Title = "Пятнашки";
        public static ConsoleColor TileBackgroundColor = ConsoleColor.DarkGreen;
        public static ConsoleColor TileForegroundColor = ConsoleColor.DarkCyan;
        public static ConsoleColor BorderBackgroundColor = ConsoleColor.DarkRed;
        public static ConsoleColor BorderForegroundColor = ConsoleColor.DarkYellow;
        public static ConsoleColor HelpBackgroundColor = ConsoleColor.Black;
        public static ConsoleColor HelpTextColor = ConsoleColor.White;
        public static ConsoleColor TileNumber = ConsoleColor.White;
        public const bool GenVizualization = false;
        public const bool RandomColor = true;
        public const int RandomizeSteps = 1000;
        public const int GameSpeed = 25;
        public const int HelpWidth = 17;
        public const int DefaultTileWidth = 11;
        public const int DefaultTileHeight = 7;
        public const int ConsoleHeight = 33;
        public const int ConsoleWidth = 68;
        public static string[] HelpMessage = { "Пятнашки", 
                                               " ",
                                               " ",
                                               " ",
                                               "Разработаны",
                                               "Анной Ивановой", 
                                               " ",
                                               " ",
                                               " ",
                                               "Для управления",
                                               "используйте",
                                               "стрелки",
                                               "← ↑ → ↓",
                                               " ",
                                               " ",
                                               " ",
                                               "Для изменения",
                                               "цветовой схемы",
                                               "нажмите пробел",
                                               " ",
                                               " ",
                                               " ",
                                               "Приятной игры!"};
        public static string[,] Numbers = {
                                           {"   ▄█    ",
                                            "    █    ",
                                            "    █    ",
                                            "    █    ",
                                            "   ▀▀▀   "},
                                           {"  ▀▀▀▀█  ",
                                            "      █  ",
                                            "  █▀▀▀▀  ",
                                            "  █      ",
                                            "  ▀▀▀▀▀  "},
                                           {"  ▀▀▀▀█  ",
                                            "      █  ",
                                            "   ▀▀▀█  ",
                                            "      █  ",
                                            "  ▀▀▀▀▀  "},
                                           {"  █   █  ",
                                            "  █   █  ",
                                            "  ▀▀▀▀█  ",
                                            "      █  ",
                                            "      ▀  "},
                                           {"  █▀▀▀▀  ",
                                            "  █      ",
                                            "  ▀▀▀▀█  ",
                                            "      █  ",
                                            "  ▀▀▀▀▀  "},
                                           {"  █▀▀▀▀  ",
                                            "  █      ",
                                            "  █▀▀▀█  ",
                                            "  █   █  ",
                                            "  ▀▀▀▀▀  "},
                                           {"  ▀▀▀▀█  ",
                                            "      █  ",
                                            "      █  ",
                                            "      █  ",
                                            "      ▀  "},
                                           {"  █▀▀▀█  ",
                                            "  █   █  ",
                                            "  █▀▀▀█  ",
                                            "  █   █  ",
                                            "  ▀▀▀▀▀  "},
                                           {"  █▀▀▀█  ",
                                            "  █   █  ",
                                            "  ▀▀▀▀█  ",
                                            "      █  ",
                                            "  ▀▀▀▀▀  "},
                                           {" █ █▀▀▀█ ",
                                            " █ █   █ ",
                                            " █ █   █ ",
                                            " █ █   █ ",
                                            " ▀ ▀▀▀▀▀ "},
                                           {" ▄█  ▄█  ",
                                            "  █   █  ",
                                            "  █   █  ",
                                            "  █   █  ",
                                            " ▀▀▀ ▀▀▀ "},
                                           {" █ ▀▀▀▀█ ",
                                            " █     █ ",
                                            " █ █▀▀▀▀ ",
                                            " █ █     ",
                                            " ▀ ▀▀▀▀▀ "},
                                           {" █ ▀▀▀▀█ ",
                                            " █     █ ",
                                            " █  ▀▀▀█ ",
                                            " █     █ ",
                                            " ▀ ▀▀▀▀▀ "},
                                           {" █ █   █ ",
                                            " █ █   █ ",
                                            " █ ▀▀▀▀█ ",
                                            " █     █ ",
                                            " ▀     ▀ "},
                                           {" █ █▀▀▀▀ ",
                                            " █ █     ",
                                            " █ ▀▀▀▀█ ",
                                            " █     █ ",
                                            " ▀ ▀▀▀▀▀ "}};
        public static void RandomColors()
        {
            Random generator = new Random(Convert.ToInt32(DateTime.Now.Ticks % int.MaxValue));
            TileBackgroundColor = (ConsoleColor)generator.Next(16);
            TileForegroundColor = (ConsoleColor)generator.Next(16);
            BorderBackgroundColor = (ConsoleColor)generator.Next(16);
            BorderForegroundColor = (ConsoleColor)generator.Next(16);
            HelpBackgroundColor = (ConsoleColor)generator.Next(16);
            HelpTextColor = (ConsoleColor)generator.Next(16);
            TileNumber = (ConsoleColor)generator.Next(16);
        }
    }
}
