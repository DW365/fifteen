﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fifteen
{
    class Program
    {
        static void Main(string[] args)
        {
            if(Globals.RandomColor) Globals.RandomColors();
            Drawing.Init();
            Game game = new Game();
            game.Start();
        }
    }
}
