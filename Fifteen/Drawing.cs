﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fifteen
{
    static class Drawing
    {

        public static void Init()
        {
            SetConsoleSize();
            Console.Title = Globals.Title;
            DrawBorder();
            DrawHelpScreen();
        }
        private static void SetConsoleSize()
        {
            Console.SetWindowPosition(0, 0);
            Console.SetWindowSize(Globals.ConsoleWidth, Globals.ConsoleHeight);
            Console.SetBufferSize(Globals.ConsoleWidth, Globals.ConsoleHeight);
        }
        private static void DrawBorder()
        {
            Console.BackgroundColor = Globals.BorderBackgroundColor;
            Console.ForegroundColor = Globals.BorderForegroundColor;
            for (int i = 0; i < (Globals.DefaultTileHeight+1)*4 + 1; i++)
            {
                Console.SetCursorPosition(0,i);
                for (int j = 0; j < (Globals.DefaultTileWidth + 1) * 4 + 3; j++)
                {
                    if (j == 0 || j == (Globals.DefaultTileWidth + 1) * 4 + 2) Console.Write("█");
                    else if (i == 0) Console.Write("▀");
                    else if (i == (Globals.DefaultTileHeight+1) * 4) Console.Write("▄");
                    else Console.Write(" ");
                }
            }
        }

        private static void DrawHelpScreen()
        {
            Console.SetBufferSize(Globals.ConsoleWidth+1, Globals.ConsoleHeight);
            Console.BackgroundColor = Globals.HelpBackgroundColor;
            Console.ForegroundColor = Globals.BorderForegroundColor;
            for (int i = 0; i < (Globals.DefaultTileHeight + 1) * 4 + 1; i++)
            {
                Console.SetCursorPosition((Globals.DefaultTileWidth + 1) * 4 + 3, i);
                for (int j = 0; j < Globals.HelpWidth; j++)
                {
                    if (j == Globals.HelpWidth - 1) Console.Write("█");
                    else if (i == 0) Console.Write("▀");
                    else if (i == (Globals.DefaultTileHeight + 1) * 4) Console.Write("▄");
                    else Console.Write(" ");
                }
            }
            WriteHelpText();
            Console.SetBufferSize(Globals.ConsoleWidth, Globals.ConsoleHeight);
        }

        private static void WriteHelpText()
        {
            Console.BackgroundColor = Globals.HelpBackgroundColor;
            Console.ForegroundColor = Globals.HelpTextColor;
            for (int i = 0; i < Globals.HelpMessage.Length; i++ )
            {
                Console.SetCursorPosition((Globals.DefaultTileWidth + 2) * 4, i+2);
                Console.Write(Globals.HelpMessage[i]);
            }
                
        }
        public static void DrawTile(int x, int y, int number, int height = Globals.DefaultTileHeight, int width = Globals.DefaultTileWidth)
        {
            Console.BackgroundColor = Globals.TileBackgroundColor;
            if (number == 16) Console.BackgroundColor = Globals.BorderBackgroundColor;
            Console.ForegroundColor = Globals.TileForegroundColor;
            for(int i = 0; i < height; i++)
            {
                Console.SetCursorPosition(x, y+i);
                for(int j = 0; j < width; j++)
                {
                    if (number == 16)
                    {
                        Console.Write(" ");
                        continue;
                    }
                    if (j == 0 || j == width - 1) Console.Write("█");
                    else if (i == 0) Console.Write("▀");
                    else if (i == height - 1) Console.Write("▄");
                    else Console.Write(" ");
                }
            }
            if(number != 16) DrawNumber(x, y, number - 1);
        }
        private static void DrawNumber(int x, int y, int number)
        {
            Console.ForegroundColor = Globals.TileNumber;
            for (int i = 0; i < Globals.DefaultTileHeight - 2; i++)
            {
                Console.SetCursorPosition(x + 1, y + i + 1);
                Console.Write(Globals.Numbers[number, i]);
            }
        }
        public static void VictoryScreen()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.SetCursorPosition(0, 12);
            string message = @"
              ╔══╗  ╔╗  ╔╗   ╔╗╔╗ ╔╗╔╗  ╔══╗  ╔══╗   ╔╗
              ╚╗╔╝  ║║  ║║   ║║║║ ║║║║  ║╔╗║  ║╔╗║   ║║
               ║║   ║╚═╗║║   ║╚╝║ ║╚╝║  ║║║║  ║║║║   ║║
               ║║   ║╔╗║║║   ╚═╗║ ╚═╗║  ║║║║  ║║║║   ╚╝
               ║║   ║╚╝║║║     ║║ ╔═╝║ ╔╝╚╝╚╗ ║╚╝║   ╔╗
               ╚╝   ╚══╝╚╝     ╚╝ ╚══╝ ╚╝  ╚╝ ╚══╝   ╚╝";
            Console.WriteLine(message);
            Console.ReadKey();
            System.Environment.Exit(0);
        }
        public static void Move(int fromX, int fromY, int toX, int toY)
        {
            Console.BackgroundColor = Globals.BorderBackgroundColor;
            int stepX = 0, stepY = 0;
            if (fromX != toX)
            {
                if (fromX < toX) stepX = 1;
                else stepX = -1;
            }
            if (fromY != toY)
            {
                if (fromY < toY) stepY = 1;
                else stepY = -1;
            }
            int cyrX = fromX * (Globals.DefaultTileWidth + 1) + 2, 
                cyrY = fromY * (Globals.DefaultTileHeight + 1) + 1;
            toX = toX * (Globals.DefaultTileWidth + 1) + 2;
            toY = toY * (Globals.DefaultTileHeight + 1) + 1;
            while (cyrY != toY || cyrX != toX)
            {
                Console.MoveBufferArea(cyrX, cyrY,Globals.DefaultTileWidth, Globals.DefaultTileHeight,cyrX+stepX,cyrY+stepY);
                System.Threading.Thread.Sleep(Globals.GameSpeed);
                cyrY += stepY;
                cyrX += stepX;
            }
        }
    }
}
